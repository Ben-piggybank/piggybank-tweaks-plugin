<?php
/*
Plugin Name: Piggybank Tweaks
Plugin URI:  https://www.piggybankmarketing.com/
Description: This plugin contains a few various minor tweaks to make WordPress more awesome.
Version:     20170529
Author:      Piggybank Marketing
Author URI:  https://www.piggybankmarketing.com/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


//* Add style sheet and javascript to customize login screen
function piggybank_login_customization() {
  wp_enqueue_style( 'custom-login', plugins_url() . '/piggybank-tweaks/login-style.css' );
	wp_enqueue_script( 'custom-login', plugins_url() . '/piggybank-tweaks/custom-login.js' );
}
add_action( 'login_enqueue_scripts', 'piggybank_login_customization' );


//* Add the filter and function, returning the widget title only if the first character is not "!"
function remove_widget_title( $widget_title ) {
  if ( substr ( $widget_title, 0, 1 ) == '!' )
  return;
  else
  return ( $widget_title );
}
add_filter( 'widget_title', 'remove_widget_title' );


//* Make shortcodes work in widgets
add_filter('widget_text', 'do_shortcode');


// disable html stripping from user bio field
remove_filter('pre_user_description', 'wp_filter_kses');

?>
